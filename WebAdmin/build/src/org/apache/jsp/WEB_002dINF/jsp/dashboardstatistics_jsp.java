package org.apache.jsp.WEB_002dINF.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class dashboardstatistics_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

static private org.apache.jasper.runtime.ProtectedFunctionMapper _jspx_fnmap_0;

static {
  _jspx_fnmap_0= org.apache.jasper.runtime.ProtectedFunctionMapper.getMapForFunction("fn:replace", org.apache.taglibs.standard.functions.Functions.class, "replace", new Class[] {java.lang.String.class, java.lang.String.class, java.lang.String.class});
}

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  static {
    _jspx_dependants = new java.util.Vector(1);
    _jspx_dependants.add("/WEB-INF/jsp/common/taglibs.jsp");
  }

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_set_var_value_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_layout$1render_title_name;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_layout$1component_name;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_varStatus_var_items;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_if_test;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_set_var_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_layout$1render_title_name = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_layout$1component_name = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_forEach_varStatus_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_if_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_set_var_value_nobody.release();
    _jspx_tagPool_s_layout$1render_title_name.release();
    _jspx_tagPool_s_layout$1component_name.release();
    _jspx_tagPool_c_forEach_varStatus_var_items.release();
    _jspx_tagPool_c_if_test.release();
    _jspx_tagPool_c_forEach_var_items.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n\n\n\n\n\n");
      if (_jspx_meth_c_set_0(_jspx_page_context))
        return;
      out.write('\n');
      out.write('\n');
      if (_jspx_meth_s_layout$1render_0(_jspx_page_context))
        return;
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_set_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_0.setPageContext(_jspx_page_context);
    _jspx_th_c_set_0.setParent(null);
    _jspx_th_c_set_0.setVar("contextPath");
    _jspx_th_c_set_0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_set_0 = _jspx_th_c_set_0.doStartTag();
    if (_jspx_th_c_set_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_0);
      return true;
    }
    _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_0);
    return false;
  }

  private boolean _jspx_meth_s_layout$1render_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:layout-render
    net.sourceforge.stripes.tag.layout.LayoutRenderTag _jspx_th_s_layout$1render_0 = (net.sourceforge.stripes.tag.layout.LayoutRenderTag) _jspx_tagPool_s_layout$1render_title_name.get(net.sourceforge.stripes.tag.layout.LayoutRenderTag.class);
    _jspx_th_s_layout$1render_0.setPageContext(_jspx_page_context);
    _jspx_th_s_layout$1render_0.setParent(null);
    _jspx_th_s_layout$1render_0.setName("/WEB-INF/jsp/common/layoutmain.jsp");
    _jspx_th_s_layout$1render_0.setDynamicAttribute(null, "title", new String("Dashboard Statistics"));
    int _jspx_eval_s_layout$1render_0 = _jspx_th_s_layout$1render_0.doStartTag();
    if (_jspx_eval_s_layout$1render_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_layout$1render_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_layout$1render_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_layout$1render_0.doInitBody();
      }
      do {
        out.write("\n    ");
        if (_jspx_meth_s_layout$1component_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_layout$1render_0, _jspx_page_context))
          return true;
        out.write("\n\n    ");
        if (_jspx_meth_s_layout$1component_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_layout$1render_0, _jspx_page_context))
          return true;
        out.write("\n\n    ");
        if (_jspx_meth_s_layout$1component_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_layout$1render_0, _jspx_page_context))
          return true;
        out.write('\n');
        int evalDoAfterBody = _jspx_th_s_layout$1render_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_layout$1render_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_layout$1render_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_layout$1render_title_name.reuse(_jspx_th_s_layout$1render_0);
      return true;
    }
    _jspx_tagPool_s_layout$1render_title_name.reuse(_jspx_th_s_layout$1render_0);
    return false;
  }

  private boolean _jspx_meth_s_layout$1component_0(javax.servlet.jsp.tagext.JspTag _jspx_th_s_layout$1render_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:layout-component
    net.sourceforge.stripes.tag.layout.LayoutComponentTag _jspx_th_s_layout$1component_0 = (net.sourceforge.stripes.tag.layout.LayoutComponentTag) _jspx_tagPool_s_layout$1component_name.get(net.sourceforge.stripes.tag.layout.LayoutComponentTag.class);
    _jspx_th_s_layout$1component_0.setPageContext(_jspx_page_context);
    _jspx_th_s_layout$1component_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_layout$1render_0);
    _jspx_th_s_layout$1component_0.setName("head");
    int _jspx_eval_s_layout$1component_0 = _jspx_th_s_layout$1component_0.doStartTag();
    if (_jspx_eval_s_layout$1component_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      out.write("\n        <link href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/css/jquery.treeTable.css\" rel=\"stylesheet\" type=\"text/css\" />\n\n        <!-- Hack to fix CSS spacing conflict between tablesorter and bootstrap -->\n        <style type=\"text/css\">\n            .header div {\n            \tfloat: left;\n            }\n        </style>\n    ");
    }
    if (_jspx_th_s_layout$1component_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_layout$1component_name.reuse(_jspx_th_s_layout$1component_0);
      return true;
    }
    _jspx_tagPool_s_layout$1component_name.reuse(_jspx_th_s_layout$1component_0);
    return false;
  }

  private boolean _jspx_meth_s_layout$1component_1(javax.servlet.jsp.tagext.JspTag _jspx_th_s_layout$1render_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:layout-component
    net.sourceforge.stripes.tag.layout.LayoutComponentTag _jspx_th_s_layout$1component_1 = (net.sourceforge.stripes.tag.layout.LayoutComponentTag) _jspx_tagPool_s_layout$1component_name.get(net.sourceforge.stripes.tag.layout.LayoutComponentTag.class);
    _jspx_th_s_layout$1component_1.setPageContext(_jspx_page_context);
    _jspx_th_s_layout$1component_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_layout$1render_0);
    _jspx_th_s_layout$1component_1.setName("body");
    int _jspx_eval_s_layout$1component_1 = _jspx_th_s_layout$1component_1.doStartTag();
    if (_jspx_eval_s_layout$1component_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      out.write("\n        <div id=\"errorAlert\" class=\"alert alert-danger hide fade in\" data-alert=\"alert\" style= \"margin-left:auto; margin-right:auto;\">\n            <a class=\"close\" data-dismiss=\"alert\" href=\"#\">&times;</a>\n            <p><strong>Error connecting to Server</strong>. Refresh the page or <a href=\"Index.action\">Login</a></p>\n        </div>\n\n        <ul id=\"myTab\" class=\"nav nav-tabs\">\n            <li class=\"active\"><a id=\"current\" href=\"#\" data-toggle=\"tab\">Current Statistics</a></li>\n            <li><a id=\"lifetime\" href=\"#\" data-toggle=\"tab\">Lifetime Statistics</a></li>\n        </ul>\n\n        <table class=\"table table-striped table-bordered table-condensed tablesorter\" style=\"width: 98%;\" id=\"treeTable\">\n            <thead>\n                <tr>\n                    <th>Name</th>\n                    <th>Status</th>\n                    <th>Received</th>\n                    <th>Filtered</th>\n                    <th>Queued</th>\n                    <th>Sent</th>\n                    <th>Errored</th>\n                </tr>\n");
      out.write("            </thead>\n            <tbody>\n                ");
      if (_jspx_meth_c_forEach_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_layout$1component_1, _jspx_page_context))
        return true;
      out.write("\n            </tbody>\n        </table>\n    ");
    }
    if (_jspx_th_s_layout$1component_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_layout$1component_name.reuse(_jspx_th_s_layout$1component_1);
      return true;
    }
    _jspx_tagPool_s_layout$1component_name.reuse(_jspx_th_s_layout$1component_1);
    return false;
  }

  private boolean _jspx_meth_c_forEach_0(javax.servlet.jsp.tagext.JspTag _jspx_th_s_layout$1component_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_varStatus_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_layout$1component_1);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${actionBean.dashboardStatusList}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("dashboardStatus");
    _jspx_th_c_forEach_0.setVarStatus("status");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n                    <tr id=\"node-");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${status.index}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">\n                        <td class=\"parent\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${dashboardStatus.name}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${dashboardStatus.state}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${dashboardStatus.statistics[RECEIVED]}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${dashboardStatus.statistics[FILTERED]}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${dashboardStatus.queued}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${dashboardStatus.statistics[SENT]}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                        <td class=\"errors\" ");
          if (_jspx_meth_c_if_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write('>');
          out.write(' ');
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${dashboardStatus.statistics[ERROR]}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                    </tr>\n\n                    ");
          if (_jspx_meth_c_forEach_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write("\n                ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_varStatus_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_if_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_0.setPageContext(_jspx_page_context);
    _jspx_th_c_if_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_0);
    _jspx_th_c_if_0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${dashboardStatus.statistics[ERROR] != 0}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_0 = _jspx_th_c_if_0.doStartTag();
    if (_jspx_eval_c_if_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write(" style=\"background-color:LightPink;\" ");
        int evalDoAfterBody = _jspx_th_c_if_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
    return false;
  }

  private boolean _jspx_meth_c_forEach_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_1 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_1.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_0);
    _jspx_th_c_forEach_1.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${dashboardStatus.childStatuses}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_1.setVar("childStatus");
    int[] _jspx_push_body_count_c_forEach_1 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_1 = _jspx_th_c_forEach_1.doStartTag();
      if (_jspx_eval_c_forEach_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n                        ");
          if (_jspx_meth_c_set_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_1, _jspx_page_context, _jspx_push_body_count_c_forEach_1))
            return true;
          out.write("\n                        ");
          if (_jspx_meth_c_set_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_1, _jspx_page_context, _jspx_push_body_count_c_forEach_1))
            return true;
          out.write("\n\n                        <tr id=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${trimName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write('-');
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${status.index}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\" class=\"child-of-node-");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${status.index}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write(" expand-child\">\n                            <td class=\"child\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${childStatus.name}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                            <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${childStatus.state}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                            <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${childStatus.statistics[RECEIVED]}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                            <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${childStatus.statistics[FILTERED]}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                            <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${childStatus.queued}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                            <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${childStatus.statistics[SENT]}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                            <td class=\"errors\" ");
          if (_jspx_meth_c_if_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_1, _jspx_page_context, _jspx_push_body_count_c_forEach_1))
            return true;
          out.write('>');
          out.write(' ');
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${childStatus.statistics[ERROR]}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n                        </tr>\n                    ");
          int evalDoAfterBody = _jspx_th_c_forEach_1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_1[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_1.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_1.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_1);
    }
    return false;
  }

  private boolean _jspx_meth_c_set_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_1, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_1)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_1 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_1.setPageContext(_jspx_page_context);
    _jspx_th_c_set_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_1);
    _jspx_th_c_set_1.setVar("childName");
    _jspx_th_c_set_1.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${childStatus.name}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_set_1 = _jspx_th_c_set_1.doStartTag();
    if (_jspx_th_c_set_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_1);
      return true;
    }
    _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_1);
    return false;
  }

  private boolean _jspx_meth_c_set_2(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_1, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_1)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_2 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_2.setPageContext(_jspx_page_context);
    _jspx_th_c_set_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_1);
    _jspx_th_c_set_2.setVar("trimName");
    _jspx_th_c_set_2.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${fn:replace(childName,' ','-')}", java.lang.Object.class, (PageContext)_jspx_page_context, _jspx_fnmap_0));
    int _jspx_eval_c_set_2 = _jspx_th_c_set_2.doStartTag();
    if (_jspx_th_c_set_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_2);
      return true;
    }
    _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_2);
    return false;
  }

  private boolean _jspx_meth_c_if_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_1, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_1)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_1 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_1.setPageContext(_jspx_page_context);
    _jspx_th_c_if_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_1);
    _jspx_th_c_if_1.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${childStatus.statistics[ERROR] != 0}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_1 = _jspx_th_c_if_1.doStartTag();
    if (_jspx_eval_c_if_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write(" style=\"background-color:LightPink;\" ");
        int evalDoAfterBody = _jspx_th_c_if_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
    return false;
  }

  private boolean _jspx_meth_s_layout$1component_2(javax.servlet.jsp.tagext.JspTag _jspx_th_s_layout$1render_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:layout-component
    net.sourceforge.stripes.tag.layout.LayoutComponentTag _jspx_th_s_layout$1component_2 = (net.sourceforge.stripes.tag.layout.LayoutComponentTag) _jspx_tagPool_s_layout$1component_name.get(net.sourceforge.stripes.tag.layout.LayoutComponentTag.class);
    _jspx_th_s_layout$1component_2.setPageContext(_jspx_page_context);
    _jspx_th_s_layout$1component_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_layout$1render_0);
    _jspx_th_s_layout$1component_2.setName("scripts");
    int _jspx_eval_s_layout$1component_2 = _jspx_th_s_layout$1component_2.doStartTag();
    if (_jspx_eval_s_layout$1component_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      out.write("\n        <script type=\"text/javascript\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/js/jquery.treeTable.js\"></script>\n        <script type=\"text/javascript\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/js/persist-min.js\"></script>\n        <script type=\"text/javascript\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/js/jquery.tablesorter.min.js\"></script>\n        <script type=\"text/javascript\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/js/jquery.tablesorter.widgets.min.js\"></script>\n\n        <!-- Script to update stats dynamically every x seconds via ajax -->\n        <script type=\"text/javascript\">\n            var timeout = 5000;\n            var showLifetimeStats = false;\n            var updateTimeout;\n\n            function updateStats() {\n                $.get('DashboardStatistics.action?getStats&showLifetimeStats=' + showLifetimeStats, function(nodes) {\n\t\t\t\t\tvar showAlert = ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${actionBean.showAlert}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(";\n\n                    // Refresh\n                    if (nodes == \"0\") {\n                        document.location.reload(true);\n                        return;\n                    }\n\n                    if (showAlert) {\n                        $(\"#errorAlert\").removeClass('hide');\n                    } else {\n                        $(\"#errorAlert\").addClass('hide');\n                    }\n\n                    for (var i = 0; i < nodes.length; i++) {\n                        var node = nodes[i];\n                        var row = $('#' + node.id);\n\n                        checkAndUpdateError(node);\n\n                        row.children().eq(1).text(node.status);\n                        row.children().eq(2).text(node.received);\n                        row.children().eq(3).text(node.filtered);\n                        row.children().eq(4).text(node.queued);\n                        row.children().eq(5).text(node.sent);\n                        row.children().eq(6).text(node.errored);\n                    }\n                    \n");
      out.write("                    $('.result').html(nodes);\n                }, \"json\");\n                \n                updateTimeout = setTimeout(updateStats, timeout);\n            }\n\n            function checkAndUpdateError(node) {\n                var row = $('#' + node.id);\n\n                if (node.errored > 0) {\n                    row.children().eq(6).css(\"background-color\", \"LightPink\");\n                } else {\n                    // Even rows paint cell background transparent\n                    if (row.index() % 2 == 0) {\n                        row.children().eq(6).css(\"background-color\", \"#F9F9F9\");\n                    }\n\n                    // Odd rows paint cell background grey\n                    else {\n                        row.children().eq(6).css(\"background-color\", \"transparent\");\n                    }\n                }\n            }\n\n            $(document).ready(function() {\n                updateTimeout = setTimeout(updateStats, timeout);\n            });\n        </script>\n\n        <!-- Enable Bootstrap Javascript Tabs -->\n");
      out.write("        <script type=\"text/javascript\">\n            $(document).ready(function() {\n                $('#myTab a').click(function(e) {\n                    e.preventDefault();\n\n                    if ($(this).attr(\"id\") == \"current\") {\n                        showLifetimeStats = false;\n                        clearTimeout(updateTimeout);\n                    } else {\n                        showLifetimeStats = true;\n                        clearTimeout(updateTimeout);\n                    }\n                    updateStats();\n                    $(this).tab('show');\n                });\n            });\n        </script>\n\n        <!-- TreeTable plugin -->\n        <script type=\"text/javascript\">\n            $(document).ready(function() {\n                $(\"#treeTable\").treeTable({\n                    initialState : \"collapsed\",\n                    clickableNodeNames : true,\n                    persist : true\n                // Persist node expanded/collapsed state\n                });\n            });\n        </script>\n\n");
      out.write("        <!-- TableSorter plugin -->\n        <script type=\"text/javascript\">\n            $(document).ready(function() {\n                $(\"#treeTable\").tablesorter({\n                    // Persist sorting state\n                    widgets : [ \"saveSort\" ],\n\n                    // Override tablesorter CSS to use bootstrap styling\n                    cssHeader : \"header\",\n                    cssAsc : \"headerSortDown\",\n                    cssDesc : \"headerSortUp\"\n                });\n            });\n        </script>\n\n        <!-- Script to highlight errored cells pink whenever value > 0 -->\n        <script type=\"text/javascript\">\n            $(document).ready(function() {\n                if (Number($(this).text()) > 0) {\n                    $(this).css(\"background-color\", \"LightPink\");\n                }\n            });\n        </script>\n\n        <!-- Hack to fix CSS extra arrow conflict between tablesorter and bootstrap -->\n        <script type=\"text/javascript\">\n            $(document).ready(function() {\n                $(\"#body table thead tr\").removeAttr(\"class\");\n");
      out.write("            });\n        </script>\n    ");
    }
    if (_jspx_th_s_layout$1component_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_layout$1component_name.reuse(_jspx_th_s_layout$1component_2);
      return true;
    }
    _jspx_tagPool_s_layout$1component_name.reuse(_jspx_th_s_layout$1component_2);
    return false;
  }
}
